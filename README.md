
# Hello, I am Felipe Leonardo, be welcome!

<a href="https://github.com/FelipeLeonardo98"> Here you can see my GitHub account and past projects! </a> <br>

<div align="center">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=felipeleonardo98&show_icons=true&theme=dracula&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=felipeleonardo98&layout=compact&langs_count=7&theme=dracula"/>
</div>

<div style="display: inline_block"> Some technologies of my daily basis are: <br>
  <img align="center" alt="Python" height="30" width="40" src="https://skillicons.dev/icons?i=python">
  <img align="center" alt="APIs with Flask" height="30" width="40" src="https://skillicons.dev/icons?i=flask">
  <img align="center" alt="Docker" height="30" width="40" src="https://skillicons.dev/icons?i=docker">
  <img align="center" alt="Git" height="30" width="40" src="https://skillicons.dev/icons?i=git"> <br>

  DevOps/DataOps skills <br>
  <img align="center" alt="Git Actions" height="30" width="40" src="https://skillicons.dev/icons?i=githubactions">
  <img align="center" alt="Prometheus" height="30" width="40" src="https://skillicons.dev/icons?i=prometheus,grafana,terraform&perline=4">
  <img align="center" alt="Grafana" height="30" width="40" src="https://skillicons.dev/icons?i=grafana,terraform">
  <img align="center" alt="Terraform" height="30" width="40" src="https://skillicons.dev/icons?i=terraform"> <br>

  Main clouds, including several services, such as: GCP (Bigquery, Cloud Functions etc) and AWS (RDS, EC2 etc) <br>
  <img align="center" alt="Cloud environments" height="30" width="40" src="https://skillicons.dev/icons?i=aws,gcp&perline=2"> <br>
  
  I am certified by both: <br>
  [AWS - Solutions Cloud Architect Associate](https://www.credly.com/badges/ce9a9c68-8ed8-4168-93a5-0ed322186d99?source=linked_in_profile) <br>
  [Google Cloud Professional Data Engineer](https://www.credential.net/8f1b33df-b089-4cc3-aecc-f592b0d9aea4)
  
  ## Keep in touch with me!
 
<div> 
  <a href = "mailto:felipeferrazleonardo98@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/felipeferrazleonardo/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
 
</div>